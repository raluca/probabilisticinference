#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Coursework in Python 
from IDAPICourseworkLibrary import *
from numpy import *
#
# Coursework 1 begins here
#
# Function to compute the prior distribution of the variable root from the data set
def Prior(theData, root, noStates):
    prior = zeros((noStates[root]), float )
# Coursework 1 task 1 should be inserted here
    for x in range(0, noDataPoints):
        prior[theData[x,root]] += 1/float(noDataPoints)
# end of Coursework 1 task 1
    return prior
# Function to compute a CPT with parent node varP and xchild node varC from the data array
# it is assumed that the states are designated by consecutive integers starting with 0
def CPT(theData, varC, varP, noStates):
    cPT = zeros((noStates[varC], noStates[varP]), float )
# Coursework 1 task 2 should be inserte4d here
    for row in theData:
        cPT[row[varC], row[varP]] += 1
        
    transposeCPT = cPT.transpose()
    
    for column in transposeCPT:
        colSum = sum(column)
        if colSum:
            column /= colSum
    
# end of coursework 1 task 2
    return cPT
# Function to calculate the joint probability table of two variables in the data set
def JPT(theData, varRow, varCol, noStates):
    jPT = zeros((noStates[varRow], noStates[varCol]), float )
#Coursework 1 task 3 should be inserted here 
    for row in theData:
        jPT[row[varRow], row[varCol]] += 1/float(noDataPoints)
# end of coursework 1 task 3
    return jPT
#
# Function to convert a joint probability table to a conditional probability table
def JPT2CPT(aJPT):
#Coursework 1 task 4 should be inserted here 
   transposeJPT = aJPT.transpose()
   for column in transposeJPT:
       colSum = sum(column)
       if colSum:
                column /= colSum
# coursework 1 taks 4 ends here
   return aJPT

#
# Function to query a naive Bayesian network
def Query(theQuery, naiveBayes): 
    rootPdf = zeros((naiveBayes[0].shape[0]), float)
# Coursework 1 task 5 should be inserted here
    prior = naiveBayes[0]
    for index, pdf in enumerate(rootPdf):
        rootPdf[index] = prior[index]
        for i in range(0, len(theQuery)):
            rootPdf[index] *= naiveBayes[i+1][theQuery[i], index]
    rootPdf /= sum(rootPdf) 
# end of coursework 1 task 5
    return rootPdf
#
# End of Coursework 1
#
# Coursework 2 begins here
#
# Calculate the mutual information from the joint probability table of two variables
def MutualInformation(jP):
    mi=0.0
# Coursework 2 task 1 should be inserted here
   

# end of coursework 2 task 1
    return mi
#
# construct a dependency matrix for all the variables
def DependencyMatrix(theData, noVariables, noStates):
    MIMatrix = zeros((noVariables,noVariables))
# Coursework 2 task 2 should be inserted here
    

# end of coursework 2 task 2
    return MIMatrix
# Function to compute an ordered list of dependencies 
def DependencyList(depMatrix):
    depList=[]
# Coursework 2 task 3 should be inserted here
    

# end of coursework 2 task 3
    return array(depList2)
#
# Functions implementing the spanning tree algorithm
# Coursework 2 task 4

def SpanningTreeAlgorithm(depList, noVariables):
    spanningTree = []
  
    return array(spanningTree)
#
# End of coursework 2
#
# Coursework 3 begins here
#
# Function to compute a CPT with multiple parents from he data set
# it is assumed that the states are designated by consecutive integers starting with 0
def CPT_2(theData, child, parent1, parent2, noStates):
    cPT = zeros([noStates[child],noStates[parent1],noStates[parent2]], float )
# Coursework 3 task 1 should be inserted here
   

# End of Coursework 3 task 1           
    return cPT
#
# Definition of a Bayesian Network
def ExampleBayesianNetwork(theData, noStates):
    arcList = [[0],[1],[2,0],[3,2,1],[4,3],[5,3]]
    cpt0 = Prior(theData, 0, noStates)
    cpt1 = Prior(theData, 1, noStates)
    cpt2 = CPT(theData, 2, 0, noStates)
    cpt3 = CPT_2(theData, 3, 2, 1, noStates)
    cpt4 = CPT(theData, 4, 3, noStates)
    cpt5 = CPT(theData, 5, 3, noStates)
    cptList = [cpt0, cpt1, cpt2, cpt3, cpt4, cpt5]
    return arcList, cptList
# Coursework 3 task 2 begins here

# end of coursework 3 task 2
#
# Function to calculate the MDL size of a Bayesian Network
def MDLSize(arcList, cptList, noDataPoints, noStates):
    mdlSize = 0.0
# Coursework 3 task 3 begins here


# Coursework 3 task 3 ends here 
    return mdlSize 
#
# Function to calculate the joint probability of a single data point in a Network
def JointProbability(dataPoint, arcList, cptList):
    jP = 1.0
# Coursework 3 task 4 begins here


# Coursework 3 task 4 ends here 
    return jP
#
# Function to calculate the MDLAccuracy from a data set
def MDLAccuracy(theData, arcList, cptList):
    mdlAccuracy=0
# Coursework 3 task 5 begins here


# Coursework 3 task 5 ends here 
    return mdlAccuracy
#
# End of coursework 2
#
# Coursework 3 begins here
#
def Mean(theData):
    realData = theData.astype(float)
    noVariables=theData.shape[1] 
    mean = []
    # Coursework 4 task 1 begins here



    # Coursework 4 task 1 ends here
    return array(mean)


def Covariance(theData):
    realData = theData.astype(float)
    noVariables=theData.shape[1] 
    covar = zeros((noVariables, noVariables), float)
    # Coursework 4 task 2 begins here


    # Coursework 4 task 2 ends here
    return covar
def CreateEigenfaceFiles(theBasis):
    adummystatement = 0 #delete this when you do the coursework
    # Coursework 4 task 3 begins here

    # Coursework 4 task 3 ends here

def ProjectFace(theBasis, theMean, theFaceImage):
    magnitudes = []
    # Coursework 4 task 4 begins here

    # Coursework 4 task 4 ends here
    return array(magnitudes)

def CreatePartialReconstructions(aBasis, aMean, componentMags):
    adummystatement = 0  #delete this when you do the coursework
    # Coursework 4 task 5 begins here

    # Coursework 4 task 5 ends here

def PrincipalComponents(theData):
    orthoPhi = []
    # Coursework 4 task 3 begins here
    # The first part is almost identical to the above Covariance function, but because the
    # data has so many variables you need to use the Kohonen Lowe method described in lecture 15
    # The output should be a list of the principal components normalised and sorted in descending 
    # order of their eignevalues magnitudes

    
    # Coursework 4 task 6 ends here
    return array(orthoPhi)

#
# main program part for Coursework 1
#
def courseWork2():
    noVariables, noRoots, noStates, noDataPoints, datain = ReadFile("HepatitisC.txt")
    theData = array(datain)
    AppendString("results.txt","Coursework One Results by Andreea-Ingrid Funie (aif109), Raluca-Andreea Bolovan (rab109)")
    AppendString("results.txt","")
    depMatrix = DependencyMatrix(theData,noVariables,noStates)
    AppendString("results.txt", "The dependency matrix for HepatitisC data set is:")
    AppendString("results.txt", depMatrix)
    depList = DependencyList(depMatrix)
    AppendString("results.txt", "The dependency list for the HepatitisC data set is:")
    AppendString("results.txt", depList)
    spanningTree = SpanningTreeAlgorithm(depList, noVariables)
    AppendString("The spanning tree found for the HepatitisC data set is", spanningTree)

def courseWork1():
    noVariables, noRoots, noStates, noDataPoints, datain = ReadFile("Neurones.txt")
    theData = array(datain)
    AppendString("results.txt","Coursework One Results by Andreea-Ingrid Funie (aif109), Raluca-Andreea Bolovan (rab109)")
    AppendString("results.txt","")
    AppendString("results.txt","The prior probability of node 0")
    # Print prior probability of node 0 (root)
    prior = Prior(theData, 0, noStates)
    AppendList("results.txt", prior)
    AppendString("results.txt","The conditional probability matrix of 2|0")
    # Print the CPT matrix for parent 0 and child 2
    cpt = CPT(theData, 2, 0, noStates)
    AppendArray("results.txt", cpt)
    AppendString("results.txt","The joint probability matrix of 2 & 0")
    # Print JPT matrix for parent 0 and child 2
    jpt = JPT(theData, 2, 0, noStates)
    AppendArray("results.txt", jpt)
    AppendString("results.txt","The CPT from JPT of 2 & 0")
    # Print CPT matrix given JPT matrix for the same parent 0 and child 2
    cjpt = JPT2CPT(jpt)
    AppendArray("results.txt", cjpt)
    # Create Bayesian network given the Prior points probabilitis and the CPT matrix, for 
    # each variable
    naiveBayes = ([Prior(theData, 0, noStates)] +
                  [CPT(theData, i, 0, noStates) for i in range(1, noVariables)])
    # Print pdf of the Bayesian network for each given query
    for theQuery in [[4, 0, 0, 0, 5], [6, 5, 2, 5, 5]]:
            AppendString("results.txt", "The pdf from the following query %s." % theQuery)
            rootPdf = Query(theQuery, naiveBayes)
            AppendList("results.txt", rootPdf)

#
# continue as described
#
#
if __name__ == '__main__':
    courseWork2()
    
