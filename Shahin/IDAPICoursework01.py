#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Coursework in Python 
from IDAPICourseworkLibrary import *
from numpy import *
#
# Coursework 1 begins here
#
# Function to compute the prior distribution of the variable root from the data set
def Prior(theData, root, noStates):
    prior = zeros((noStates[root]), float )
# Coursework 1 task 1 should be inserted here
    for row in theData:
      prior[row[root]] += 1
# end of Coursework 1 task 1
    return prior/prior.sum()
# Function to compute a CPT with parent node varP and xchild node varC from the data array
# it is assumed that the states are designated by consecutive integers starting with 0
def CPT(theData, varC, varP, noStates):
    cPT = zeros((noStates[varC], noStates[varP]), float )
# Coursework 1 task 2 should be inserte4d here
    pStates = noStates[varP]
    for row in theData:
     cPT [row[varC]][row[varP]] += 1
    for index in range(0,pStates):
     cPT.transpose()[index] = cPT.transpose()[index]/cPT.transpose()[index].sum()
# end of coursework 1 task 2
    return cPT
# Function to calculate the joint probability table of two variables in the data set
def JPT(theData, varRow, varCol, noStates):
    jPT = zeros((noStates[varRow], noStates[varCol]), float )
#Coursework 1 task 3 should be inserted here 
    for row in theData:
      jPT[row[varRow]][row[varCol]] += 1
    size = len(theData)
    trJPT = jPT.transpose()
    for index in range(0,noStates[varCol]):
     trJPT[index] = trJPT[index]/size
    jPT = trJPT.transpose()
# end of coursework 1 task 3
    return jPT
#
# Function to convert a joint probability table to a conditional probability table
def JPT2CPT(aJPT):
#Coursework 1 task 4 should be inserted here 
    trAJPT = aJPT.transpose()
    for index in range(0,len(trAJPT)):
     trAJPT[index] = trAJPT[index]/trAJPT[index].sum()
    aJPT = trAJPT.transpose()
# courseworkrootPdf 1 taks 4 ends here
    return aJPT

#
# Function to query a naive Bayesian network
def Query(theQuery, naiveBayes): 
    rootPdf = zeros((naiveBayes[0].shape[0]), float)
# Coursework 1 task 5 should be inserted here
    for i in range(0,len(naiveBayes[0])):
     rootPdf[i] = naiveBayes[0][i]
     for j in range(0,len(theQuery)):
      rootPdf[i] = rootPdf[i] * naiveBayes[j+1][theQuery[j]][i]
# end of coursework 1 task 5
    return rootPdf/sum(rootPdf)
#
# End of Coursework 1
#
# Coursework 2 begins here
#
# Calculate the mutual information from the joint probability table of two variables
def MutualInformation(jP):
    mi=0.0
# Coursework 2 task 1 should be inserted here
    #column probabilities
    pB = zeros(jP.shape[1], float)
    pB = [sum(col) for col in jP.transpose()]
    for i in range(0,len(jP)):
      #row probability
      row = jP[i]
      pA = sum(row)
      for j in range(0,len(row)):
	if pB and pA and jP[i][j]:
	 mi += jP[i][j] * math.log((jP[i][j]/(pB[j]*pA)),2)
# end of coursework 2 task 1
    return mi
#
# construct a dependency matrix for all the variables
def DependencyMatrix(theData, noVariables, noStates):
    MIMatrix = zeros((noVariables,noVariables))
# Coursework 2 task 2 should be inserted here
    for i in range(0,noVariables):
      for j in range(i,noVariables):
	jP = JPT(theData, i, j, noStates)
	MIMatrix[i][j] = MIMatrix[j][i] =  MutualInformation(jP)
# end of coursework 2 task 2
    return MIMatrix
# Function to compute an ordered list of dependencies 
def DependencyList(depMatrix):
    depList=[]
# Coursework 2 task 3 should be inserted here
    for i in range(0,len(depMatrix)):
      row = depMatrix[i]
      for j in range(i+1, len(row)):
	#build a triplet [dependency, node1, node2]
	triplet = [depMatrix[i][j],i,j]
	depList.append(triplet)	
    #sort list
    depList2 = sorted(depList,reverse=True)
# end of coursework 2 task 3
    return array(depList2)
#
# Functions implementing the spanning tree algorithm
# Coursework 2 task 4

#helper function to check if a vertex is in a sub-tree
def SubTreeIndex(sTree, vertex):
  index = -1 
  for i in range (0,len(sTree)):
    if vertex in sTree[i]:
      index = i 
      break
  return index

#Using Kruskal's algorithm to build a spanning tree
def SpanningTreeAlgorithm(depList, noVariables):
    spanningTree = []
    subTrees = []
    for lst in depList:
      nodeA = lst[1]
      nodeB = lst[2]
      #check if verticies already in a subtree
      aSubIndex = SubTreeIndex(subTrees, nodeA)
      bSubIndex = SubTreeIndex(subTrees, nodeB)
      if aSubIndex == -1 and bSubIndex == -1:
	spanningTree.append(lst)
	subTrees.append([nodeA,nodeB])
      elif aSubIndex == -1:
	spanningTree.append(lst)
	subTrees[bSubIndex].append(nodeA)
      elif bSubIndex == -1:
	spanningTree.append(lst)
	subTrees[aSubIndex].append(nodeB)
      elif aSubIndex == bSubIndex:
	continue
      else: 
	#merge subtrees 
	spanningTree.append(lst)
	subTrees[aSubIndex].extend(subTrees[bSubIndex])
	subTrees.pop(bSubIndex)
    return array(spanningTree)
#
# End of coursework 2
#
# Coursework 3 begins here
#
# Function to compute a CPT with multiple parents from he data set
# it is assumed that the states are designated by consecutive integers starting with 0
def CPT_2(theData, child, parent1, parent2, noStates):
    cPT = zeros([noStates[child],noStates[parent1],noStates[parent2]], float )
# Coursework 3 task 1 should be inserted here
   

# End of Coursework 3 task 1           
    return cPT
#
# Definition of a Bayesian Network
def ExampleBayesianNetwork(theData, noStates):
    arcList = [[0],[1],[2,0],[3,2,1],[4,3],[5,3]]
    cpt0 = Prior(theData, 0, noStates)
    cpt1 = Prior(theData, 1, noStates)
    cpt2 = CPT(theData, 2, 0, noStates)
    cpt3 = CPT_2(theData, 3, 2, 1, noStates)
    cpt4 = CPT(theData, 4, 3, noStates)
    cpt5 = CPT(theData, 5, 3, noStates)
    cptList = [cpt0, cpt1, cpt2, cpt3, cpt4, cpt5]
    return arcList, cptList
# Coursework 3 task 2 begins here

# end of coursework 3 task 2
#
# Function to calculate the MDL size of a Bayesian Network
def MDLSize(arcList, cptList, noDataPoints, noStates):
    mdlSize = 0.0
# Coursework 3 task 3 begins here


# Coursework 3 task 3 ends here 
    return mdlSize 
#
# Function to calculate the joint probability of a single data point in a Network
def JointProbability(dataPoint, arcList, cptList):
    jP = 1.0
# Coursework 3 task 4 begins here


# Coursework 3 task 4 ends here 
    return jP
#
# Function to calculate the MDLAccuracy from a data set
def MDLAccuracy(theData, arcList, cptList):
    mdlAccuracy=0
# Coursework 3 task 5 begins here


# Coursework 3 task 5 ends here 
    return mdlAccuracy
#
# End of coursework 2
#
# Coursework 3 begins here
#
def Mean(theData):
    realData = theData.astype(float)
    noVariables=theData.shape[1] 
    mean = []
    # Coursework 4 task 1 begins here



    # Coursework 4 task 1 ends herea matrix 
    return array(mean)


def Covariance(theData):
    realData = theData.astype(float)
    noVariables=theData.shape[1] 
    covar = zeros((noVariables, noVariables), float)
    # Coursework 4 task 2 begins here


    # Coursework 4 task 2 ends here
    return covar
def CreateEigenfaceFiles(theBasis):
    adummystatement = 0 #delete this when you do the coursework
    # Coursework 4 task 3 begins here

    # Coursework 4 task 3 ends here

def ProjectFace(theBasis, theMean, theFaceImage):
    magnitudes = []
    # Coursework 4 task 4 begins here

    # Coursework 4 task 4 ends here
    return array(magnitudes)

def CreatePartialReconstructions(aBasis, aMean, componentMags):
    adummystatement = 0  #delete this when you do the coursework
    # Coursework 4 task 5 begins here

    # Coursework 4 task 5 ends here

def PrincipalComponents(theData):
    orthoPhi = []
    # Coursework 4 task 3 begins here
    # The first part is almost identical to the above Covariance function, but because the
    # data has so many variables you need to use the Kohonen Lowe method described in lecture 15
    # The output should be a list of the principal components normalised and sorted in descending 
    # order of their eignevalues magnitudes

    
    # Coursework 4 task 6 ends here
    return array(orthoPhi)

#
# main program part for Coursework 1
##
noVariables, noRoots, noStates, noDataPoints, datain = ReadFile("HepatitisC.txt")
theData = array(datain)
AppendString("results2.txt","Coursework Two Results by Sahin Mir (sm6009)")
AppendString("results2.txt","") #blank line
#AppendString("results.txt","The prior probability of node 0")
#prior = Prior(theData, 0, noStates)
#AppendList("results.txt", prior)
#AppendString("results.txt","The conditional probability matrix of 2|0")
#cpt = CPT(theData, 2, 0, noStates)
#AppendArray("results.txt", cpt)
#AppendString("results.txt","The joint probability matrix of 2 & 0")
#jpt = JPT(theData, 2, 0, noStates)
#AppendArray("results.txt", jpt)
#AppendString("results.txt","The CPT from JPT of 2 & 0")
#cjpt = JPT2CPT(jpt)
#AppendArray("results.txt", cjpt)
#AppendString("results.txt","The results of query [4,0,0,0,5] on the naive network")
##creating the network
#network = [Prior(theData, 0, noStates)]
#for i in range(1,noVariables):
 #network.append(CPT(theData, i,0, noStates))
##
#theQuery = [4,0,0,0,5]
#query = Query(theQuery,network)
#AppendList("results.txt", query)
#AppendString("results.txt","The results of query [6,5,2,5,5]  on the naive network")
#theQuery = [6,5,2,5,5]
#query = Query(theQuery,network)
#AppendList("results.txt", query)
#

AppendString("results2.txt","The dependency matrix for the HepatitisC data set")
MM = DependencyMatrix(theData, noVariables, noStates)
AppendArray("results2.txt", MM)
AppendString("results2.txt","The dependency list for the HepatitisC data set")
DList = DependencyList(MM)
AppendArray("results2.txt", DList)
AppendString("results2.txt","The spanning tree found for HepatitisC data set")
STree = SpanningTreeAlgorithm(DList, noVariables) 
AppendArray("results2.txt", STree)
